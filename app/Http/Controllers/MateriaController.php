<?php

namespace App\Http\Controllers;

use App\Models\Materia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $materias = Materia::all();
        return view('materia', compact('materias'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('materiacrear');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)//guardar la base de datos el nuevo registros
    {
        $request->validate([
            'nombre' => 'required',
            'sigla' => 'required',
            'grupo' => 'required',
        ]);

        Materia::create($request->all());

        return redirect()->route('materias.index')->with('success','Materia creada satisfactoriamente.');
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Materia $materia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Materia $materia)//para abrir un formulario para edicion
    {

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Materia $materia) // para actualizar el registro
    {
        $materia->update($request->all());
        return redirect()->route('materias.index')->with('success','Materia creada satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Materia $materia)
    {
        $materia->delete();
        return redirect()->route('materias.index')->with('success','Materia eliminada satisfactoriamente.');

    }
}
