<?php

namespace App\Http\Controllers;

use App\Models\Acapite;
use App\Models\Capitulo;
use App\Models\Modalidad;
use App\Models\Subcapitulo;
use Illuminate\Http\Request;

class ModalidadController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $modalidades = Modalidad::all();
        return view('modalidad',compact('modalidades'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  */
    public function createCapitulo(Request $request)
    {
        $capitulo = Capitulo::create([
            'indice'=>$request->indice,
            'nombre'=>$request->capitulo,
            'modalidad_id'=>$request->modalidad_id
        ]);
        $capitulo->save();
        // Obtener la lista de capítulos
        $capitulosAll = Capitulo::all();
        // Devolver los datos necesarios como respuesta JSON
        return response()->json([
            'capitulos' => $capitulosAll
        ]);
    }
    public function createAcapite(Request $request)
    {
        $acapite = Acapite::create([
            'indice'=>$request->indice,
            'nombre'=>$request->acapite,
            'capitulo_id'=>$request->opciones[0]
        ]);
        
        $acapite->save();
        // Obtener la lista de capítulos
        $acapite = Acapite::all();
        // Devolver los datos necesarios como respuesta JSON
        return response()->json([
            'acapites' => $acapite
        ]);
    }
    // /**
    //  * Store a newly created resource in storage.
    //  */
    // public function store(Request $request)
    // {
    //     //
    // }

    // /**
    //  * Display the specified resource.
    //  */
    public function show(string $id)
    {
        $capitulos = Capitulo::orderBy('indice')->where('modalidad_id',$id)->get();
        // $acapite=[];
        // foreach ($capitulos as $capitulo) {
        //     $consulta = Acapite::all()->where('capitulo_id',$capitulo->id);
        //     array_push($acapite,$consulta);
        // }
        $acapite = Acapite::orderBy('indice')->get();
        $modalidad = Modalidad::all();
        
        return compact('modalidad','capitulos','acapite');
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  */
    // public function editar()
    // {
    //     $documentos = Documento::all();
    //     $capitulos = Capitulo::all();
    //     return view('documentoEdit',compact('documentos','capitulos'));
    // }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyAcapite(string $id)
    {
         // Encuentra el producto a eliminar
            $eliminar = Acapite::findOrFail($id);

            // Elimina el producto
            $eliminar->delete();

            // Redirige de vuelta a la página de productos con un mensaje
            return response()->json(['mensaje' => 'El acápite se eliminó correctamente']);;
    }
}
