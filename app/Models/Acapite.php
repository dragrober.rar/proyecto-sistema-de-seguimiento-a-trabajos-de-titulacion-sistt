<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acapite extends Model
{
    protected $fillable = ['indice', 'nombre', 'capitulo_id'];
    public function capitulo(){
        return $this->hasMany(Capitulo::class);
    }
    use HasFactory;
}
