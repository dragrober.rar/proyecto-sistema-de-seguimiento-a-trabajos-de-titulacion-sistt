<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use League\CommonMark\Node\Block\Document;
use PhpParser\Node\Expr\FuncCall;

class Capitulo extends Model
{
    use HasFactory;
    protected $fillable = ['indice', 'nombre', 'modalidad_id'];
    public function documento(){
        return $this->hasMany(Documento::class);
    }
    public function sub_capitulo(){
        return $this->belongsTo(Sub_Capitulo::class);
    }
}
