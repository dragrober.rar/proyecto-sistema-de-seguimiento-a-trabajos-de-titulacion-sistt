<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    use HasFactory;
    protected $fillable = ['nombre','detalle'];
    public function capitulo(){
        return $this->belongsTo(Capitulo::class);
    }
}
