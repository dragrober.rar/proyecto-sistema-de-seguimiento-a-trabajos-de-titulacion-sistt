<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcapitulo extends Model
{
    use HasFactory;
    protected $fillable = ['indice', 'nombre', 'documento_id'];
    public function capitulo(){
        return $this->hasMany(Capitulo::class);
    }
}
