@extends('adminlte::page')

@section('title', 'Documentos de Grado')

@section('content_header')
    <h1>Documentos de grado</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Documento de grado</h3>
        <div class="card-tools">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
 
          {{-- <a href="{{route('documento.editar')}}" class="btn btn-primary">Editar Estructura</a> --}}
        </div>
        <!-- /.card-tools -->
      </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
            <select name="documento" id="select-documento" class="btn btn-gradient btn-success">
                <option value="1" >Seleccionar documento de grado</option>
                @foreach ($documentos as $documento)
                    <option value={{$documento->id}}>{{$documento->tipoDeDocumento}}</option>
                @endforeach
                <option value=""></option>
                </select>
        </div>
        <div class="col-md-6">
            <ul class="list-unstyled" id="estructura"></ul>
        </div>
      </div>
    </div>
    <div class="card-footer">

      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>


</body>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    let selectElement = document.getElementById('select-documento');
    selectElement.addEventListener('change', function() {
        let selectedValue = selectElement.value;
        let url = '/documentos/'+selectedValue;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                const estructura = document.getElementById('estructura');
                data.capitulo.forEach(element => {
                    const li = document.createElement('li');
                    li.textContent = element.nombre;
                    estructura.appendChild(li);
                    const ul = document.createElement('ul');
                    data.subcapitulo.forEach(element1=>{
                        if(element1.capitulo_id==element.id){
                            const li1 = document.createElement('li');
                            li1.textContent=element1.nombre;
                            ul.appendChild(li1);
                        }
                    })
                    li.appendChild(ul);
                });
            
            }
            );  
    });

    const form = document.getElementById('miFormulario');
    const capituloInput = document.getElementById('capitulo');
    const opcionesSelect = document.getElementById('select-opciones');

form.addEventListener('submit', function(event) {
  event.preventDefault(); // Prevenir el envío normal del formulario

  const token = this.querySelector('input[name="_token"]').value; // Obtener el token CSRF desde el formulario

  const formData = new FormData(this); // Obtener los datos del formulario

  fetch(this.action, { // Enviar los datos del formulario a la API
    method: 'POST',
    body: formData,
    headers: {
      'X-CSRF-TOKEN': token // Incluir el token CSRF en las cabeceras de la solicitud
    }
  })
  .then(response => response.json())
  .then(data => {
    // Agregar el nuevo capítulo al select
    console.log(data.capitulos);
    opcionesSelect.innerHTML='';
    data.capitulos.forEach(element => {
        const option = document.createElement('option');
        option.value = element.id;
        option.text = element.nombre;
        opcionesSelect.add(option);
    });

    // Limpiar el campo de capítulo
    capituloInput.value = '';

    // Mostrar un mensaje de confirmación utilizando Toast de AdminLTE
    const toastBody = document.createElement('div');
    toastBody.classList.add('toast-body');
    toastBody.innerText = 'El capítulo se ha guardado correctamente.';

    const toast = document.createElement('div');
    toast.classList.add('toast');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');
    toast.setAttribute('data-delay', '5000');

    const toastHeader = document.createElement('div');
    toastHeader.classList.add('toast-header');
    toastHeader.innerHTML = `
      <i class="fas fa-check-circle mr-2"></i>
      <strong class="mr-auto">¡Éxito!</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span>
      </button>
    `;

    toast.appendChild(toastHeader);
    toast.appendChild(toastBody);

    const toastContainer = document.querySelector('.toast-container');
    toastContainer.appendChild(toast);

    const toastInstance = new bootstrap.Toast(toast);
    toastInstance.show();
  })
  .catch(error => {
    console.error(error);
  });
});
    </script>
@stop