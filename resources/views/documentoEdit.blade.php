@extends('adminlte::page')

@section('title', 'Documentos de Grado')

@section('content_header')
    <h1>Documentos de grado Edicion</h1>
   
@stop

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Documento de grado Edicion</h3>
        <div class="card-tools">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
          <!-- Botón que abre el modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Agregar Capitulo
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Agregar estructura</h4>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="miFormulario" action="{{route('documento.capitulo')}}" method="POST">
                <!-- Agrega el token CSRF en el formulario -->
                @csrf
              
                <div class="form-group">
                  <label for="capitulo">Capítulo:</label>
                  <input type="text" name="capitulo" id="capitulo" class="form-control">
                </div>
              
                <div class="form-group">
                  <label for="select-opciones">Opciones:</label>
                  <select name="opciones[]" id="select-opciones" class="form-control" multiple>
                    <!-- Aquí se agregarán las opciones dinámicamente -->
                    @foreach ($capitulos as $capitulo)
                        <option value="">{{$capitulo->nombre        }}</option>
                    @endforeach
                  </select>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Agregar Capitulo</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
       
                  </div>
              </form>
              
              <hr>
        </div>
        <div class="toast-container position-fixed top-0 end-0 p-3">
            <!-- Aquí se agregarán los mensajes de confirmación -->
          </div>
        
      </div>
    </div>
  </div>
        </div>
        <!-- /.card-tools -->
      </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
            <select name="documento" id="select-documento" class="btn btn-gradient btn-success">
                <option value="1" >Seleccionar documento de grado</option>
                @foreach ($documentos as $documento)
                    <option value={{$documento->id}}>{{$documento->tipoDeDocumento}}</option>
                @endforeach
                <option value=""></option>
                </select>
        </div>
        <div class="col-md-6">
            <div class="accordion" id="accordionExample">
              
                <div class="card" id="estructura">
                  </div>
             
              </div>
              
        </div>
      </div>
    </div>
    <div class="card-footer">
        The footer of the card
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>
</body>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    let selectElement = document.getElementById('select-documento');
    var idDocumentUnique = 0;
    selectElement.addEventListener('change', function() {
        let selectedValue = selectElement.value;
        idDocumentUnique= selectElement.value;
        console.log(idDocumentUnique);
        let url = '/documentos/'+selectedValue;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                const estructura = document.getElementById('estructura');
                data.capitulo.forEach(element => {
                const car_header = document.createElement('div');
                car_header.classList.add("car-header");
                car_header.setAttribute("id","headingOne");
                const h2 = document.createElement("h2");
                const button = document.createElement("button");
                // Agregar las clases a los elementos HTML
                h2.classList.add("mb-0");
                button.classList.add("btn", "btn-link");
                button.setAttribute("type", "button");
                button.setAttribute("data-toggle", "collapse");
                button.setAttribute("data-target", "#collapseOne");
                button.setAttribute("aria-expanded", "true");
                button.setAttribute("aria-controls", "collapseOne");
                // Agregar el texto al botón
                button.textContent = element.nombre;
                // Agregar los elementos al div
                h2.appendChild(button);
                car_header.appendChild(h2);
                 estructura.appendChild(car_header);
                 data.subcapitulo.forEach(element1=>{
                        if(element1.capitulo_id==element.id){
                            // crea el elemento div
                        var div = document.createElement("div");

                        // establece el atributo id en "collapseOne"
                        div.setAttribute("id", "collapseOne");

                        // establece la clase "collapse show"
                        div.setAttribute("class", "collapse show");

                        // crea el elemento h2
                        var h2 = document.createElement("h2");
                        h2.setAttribute("class", "mb-0");

                        // crea el elemento button
                        var button = document.createElement("button");
                        button.setAttribute("class", "btn btn-link");
                        button.setAttribute("type", "button");
                        button.setAttribute("data-toggle", "collapse");
                        button.setAttribute("data-target", "#collapseOne");
                        button.setAttribute("aria-expanded", "true");
                        button.setAttribute("aria-controls", "collapseOne");
                        button.textContent = "Panel 1";

                        // agrega el botón al elemento h2
                        h2.appendChild(button);

                        // crea el elemento div interno
                        var innerDiv = document.createElement("div");
                        innerDiv.setAttribute("class", "card-body");
                        innerDiv.textContent = element1.nombre;

                        // agrega el elemento interno al elemento div
                        div.appendChild(innerDiv);

                        // agrega el elemento div al elemento con el id "accordionExample"
                        document.getElementById("estructura").appendChild(div);

                        }
                    })

                });
            
            }
            );  
    });
   

const form = document.getElementById('miFormulario');
const capituloInput = document.getElementById('capitulo');
const opcionesSelect = document.getElementById('select-opciones');

form.addEventListener('submit', function(event) {
  event.preventDefault(); // Prevenir el envío normal del formulario

  const token = this.querySelector('input[name="_token"]').value; // Obtener el token CSRF desde el formulario

  const formData = new FormData(this); // Obtener los datos del formulario
  
  fetch(this.action, { // Enviar los datos del formulario a la API
    method: 'POST',
    body: formData,
    headers: {
      'X-CSRF-TOKEN': token // Incluir el token CSRF en las cabeceras de la solicitud
    }
  })
  .then(response => response.json())
  .then(data => {
    // Agregar el nuevo capítulo al select
    console.log(data.capitulos);
    opcionesSelect.innerHTML='';
    data.capitulos.forEach(element => {
        const option = document.createElement('option');
        option.value = element.id;
        option.text = element.nombre;
        opcionesSelect.add(option);
    });

    // Limpiar el campo de capítulo
    capituloInput.value = '';

    // Mostrar un mensaje de confirmación utilizando Toast de AdminLTE
    const toastBody = document.createElement('div');
    toastBody.classList.add('toast-body');
    toastBody.innerText = 'El capítulo se ha guardado correctamente.';

    const toast = document.createElement('div');
    toast.classList.add('toast');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');
    toast.setAttribute('data-delay', '5000');

    const toastHeader = document.createElement('div');
    toastHeader.classList.add('toast-header');
    toastHeader.innerHTML = `
      <i class="fas fa-check-circle mr-2"></i>
      <strong class="mr-auto">¡Éxito!</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span>
      </button>
    `;

    toast.appendChild(toastHeader);
    toast.appendChild(toastBody);

    const toastContainer = document.querySelector('.toast-container');
    toastContainer.appendChild(toast);

    const toastInstance = new bootstrap.Toast(toast);
    toastInstance.show();
  })
  .catch(error => {
    console.error(error);
  });
});



    </script>
@stop