@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Proyectos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Buscar Proyecto</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('hestudiantes.index') }}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar documentos...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<br>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    @if(request()->has('search') && !empty(request()->input('search')))
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <a data-toggle="collapse" href="#collapse1">
                                        <img src="/images/documentos/documento1.jpg" alt="Documento" width="800" height="600">
                                    </a>
                                    <div id="collapse1" class="collapse">
                                        <div class="card-body">
                                            <h5 class="card-title">Descripción del Documento 1</h5>
                                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac nisi ac justo lobortis finibus. Curabitur a leo ac risus commodo pharetra ac eu mauris.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Resto del código... -->
                        </div>
                    @endif
                </div>
                
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
        $(document).ready(function() {
            var showMoreButton = $('#showMoreButton');
            var imagesToToggle = $('.toggle-image');
            var isShowingAll = false;

            showMoreButton.on('click', function() {
                if (isShowingAll) {
                    imagesToToggle.hide();
                    showMoreButton.text('Mostrar más imágenes');
                } else {
                    imagesToToggle.show();
                    showMoreButton.text('Mostrar menos imágenes');
                }
                isShowingAll = !isShowingAll;
            });
        });
    </script>
@stop
