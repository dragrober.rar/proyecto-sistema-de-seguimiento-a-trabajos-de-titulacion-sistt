@extends('adminlte::page')

@section('title', 'Materias')

@section('content_header')
    <h1>Materias</h1>
@stop


@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lista de Materias</h3>
                    <div class="card-tools">
                                <a href="{{route('materias.create')}}" class="btn btn-primary">Crear Materia</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nombre</th>
                                    <th>Sigla</th>
                                    <th>Grupo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($materias as $materia)
                                <tr>
                                    <td>{{ $materia->id }}</td>
                                    <td>{{ $materia->nombre }}</td>
                                    <td>{{ $materia->sigla }}</td>
                                    <td>{{ $materia->grupo }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editarMateria{{$materia->id}}">
                                             Editar
                                        </button>
                                        
                                        <form action="{{ route('materias.destroy', $materia->id) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                        </form>
                                    </td>
                                  
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- Modal para editar una materia -->
@foreach ($materias as $materia)
<div class="modal fade" id="editarMateria{{$materia->id}}" tabindex="-1" role="dialog" aria-labelledby="editarMateria{{$materia->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editarMateria{{$materia->id}}Label">Editar materia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('materias.update', $materia->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $materia->nombre }}" required>
                    </div>
                    <div class="form-group">
                        <label for="sigla">Sigla</label>
                        <input type="text" class="form-control" id="sigla" name="sigla" value="{{ $materia->sigla }}" required>
                    </div>
                    <div class="form-group">
                        <label for="grupo">Grupo</label>
                        <input type="text" class="form-control" id="grupo" name="grupo" value="{{ $materia->grupo }}" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">
                    Guardar cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

                </div>
            </div>
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    let selectElement = document.getElementById('select-documento');
    selectElement.addEventListener('change', function() {
        let selectedValue = selectElement.value;
        let url = '/documentos/'+selectedValue;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                const estructura = document.getElementById('estructura');
                data.capitulo.forEach(element => {
                    const li = document.createElement('li');
                    li.textContent = element.nombre;
                    estructura.appendChild(li);
                    const ul = document.createElement('ul');
                    data.subcapitulo.forEach(element1=>{
                        if(element1.capitulo_id==element.id){
                            const li1 = document.createElement('li');
                            li1.textContent=element1.nombre;
                            ul.appendChild(li1);
                        }
                    })
                    li.appendChild(ul);
                });
            
            }
            );  
    });
    </script>

@stop