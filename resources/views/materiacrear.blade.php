@extends('adminlte::page')

@section('title', 'Crear Materia')

@section('content_header')
    <h1>Crear Materia</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('materias.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="sigla">Sigla</label>
                            <input type="text" name="sigla" id="sigla" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="grupo">Grupo</label>
                            <input type="text" name="grupo" id="grupo" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    @if(session('mensaje'))
        <script>
            toastr.success('{{ session('mensaje') }}');
        </script>
    @endif
@stop
