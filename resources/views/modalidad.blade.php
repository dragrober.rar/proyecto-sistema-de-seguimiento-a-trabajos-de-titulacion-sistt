@extends('adminlte::page')

@section('title', 'Documentos de Grado')

@section('content_header')
    <h1>Documentos de grado Edicion</h1>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    
@stop

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Documento de grado Edicion</h3>
        <div class="card-tools">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
          <!-- Botón que abre el modal -->
          <div id="editar">
           
          </div>
      
          <div class="modal fade" id="confirm-delete-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmar eliminación</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>¿Está seguro de que desea eliminar este acapite?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger" id="confirm-delete-btn">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Agregar Capitulo</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="miFormulario" action="{{route('modalidad.capitulo')}}" method="POST">
                <!-- Agrega el token CSRF en el formulario -->
                @csrf
                <div class="form-group">
                  <label for="capitulo">Nombre del Capítulo:</label>
                  <input type="text" name="capitulo" id="capitulo" class="form-control">
                </div>
                <div class="form-group">
                  <label for="indice">Indice del Capitulo:</label>
                  <input type="number" name="indice" id="indice" class="form-control">
                </div>
              <input type="hidden" name="modalidad_id" id="modalidad_id">
                <div class="form-group">
                  <label for="select-opciones">Opciones:</label>
                  <select name="opciones[]" id="select-opciones" class="form-control" multiple>
                    <!-- Aquí se agregarán las opciones dinámicamente -->
                  </select>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Agregar Capitulo</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
       
                  </div>
              </form>
              <hr>
        </div>
        {{-- <div class="toast-container position-fixed top-0 end-0 p-3">
            <!-- Aquí se agregarán los mensajes de confirmación -->
          </div> --}}
        
      </div>
    </div>
  </div>
   <!-- Modal 2-->
   <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Agregar Acapite</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="miFormulario1" action="{{route('modalidad.acapite')}}" method="POST">
                <!-- Agrega el token CSRF en el formulario -->
                @csrf
                <div class="form-group">
                  <label for="acapite">Nombre del Acapite:</label>
                  <input type="text" name="acapite" id="acapite" class="form-control">
                </div>
                <div class="form-group">
                  <label for="indice">Indice del Acapite:</label>
                  <input type="number" name="indice" id="indice" class="form-control">
                </div>
              
                <div class="form-group">
                  <label for="select-opciones">Seleccionar Capitulo a Agregar:</label>
                  <select name="opciones[]" id="select-acapite" class="form-control" multiple>
                    <!-- Aquí se agregarán las opciones dinámicamente -->
                  </select>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Agregar Acapite</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
       
                  </div>
              </form>
              <hr>
        </div>
        <div class="toast-container position-fixed top-0 end-0 p-3">
            <!-- Aquí se agregarán los mensajes de confirmación -->
          </div>
        
      </div>
    </div>
  </div>
        </div>
        <!-- /.card-tools -->
      </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
            <select name="documento" id="select-documento" class="btn btn-gradient btn-success">
                <option value="1" >Seleccionar documento de grado</option>
                @foreach ($modalidades as $modalidad)
                    <option value={{$modalidad->id}}>{{$modalidad->nombre}}</option>
                @endforeach
                <option value=""></option>
                </select>
        </div>
        <div class="col-md-6">
            <div class="accordion" id="accordionExample">
              
                <div class="card" id="estructura">
                  </div>
             
              </div>
              
        </div>
      </div>
    </div>
    <div class="card-footer">
        The footer of the card
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>
</body>


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    let selectElement = document.getElementById('select-documento');
    var idGlobal = 0;
    selectElement.addEventListener('change', function() {
        idGlobal = selectElement.value;
        console.log(idGlobal);
        let selectedValue = selectElement.value;
        let url = '/modalidad/'+selectedValue;
        fetch(url)
        .then(response => response.json())
        .then(data => {
            const miDiv = document.getElementById('editar');
            const button = document.createElement('button');
            const button1 = document.createElement('button');
            miDiv.innerHTML = '';
                button.innerText = 'Editar Capitulos';
                button.classList.add('btn', 'btn-gradient', 'btn-primary');
                button.setAttribute('data-toggle', 'modal');
                button.setAttribute('data-target', '#myModal');
                button.id = 'mi-boton';
                button1.innerText = 'Editar SubCapitulo';
                button1.classList.add('btn', 'btn-gradient', 'btn-info', 'mr-2');
                button1.setAttribute('data-toggle', 'modal');
                button1.setAttribute('data-target', '#myModal2');
                button1.id = 'mi-boton1';

                miDiv.appendChild(button1);
                miDiv.appendChild(button); 
                const estructura = document.getElementById('estructura');
                estructura.innerHTML = '';
                data.capitulos.forEach((element,o) => {
                  const car_header = document.createElement('div');
                  car_header.classList.add("car-header");
                  car_header.setAttribute("id","headingOne"+o);
                  const h2 = document.createElement("h2");
                  const button = document.createElement("button");
                     // Agregar las clases a los elementos HTML
                  h2.classList.add("mb-0");
                  button.classList.add("btn", "btn-link");
                  button.setAttribute("type", "button");
                  button.setAttribute("data-toggle", "collapse");
                  button.setAttribute("data-target", "#collapseOne"+o);
                  button.setAttribute("aria-expanded", "false");
                  button.setAttribute("aria-controls", "collapseOne"+o);
                      // Agregar el texto al botón
                  button.textContent = element.nombre+' '+element.indice;
                  // Agregar los elementos al div
                  h2.appendChild(button);
                  car_header.appendChild(h2);
                  estructura.appendChild(car_header);
                  var div = document.createElement("div");

                  // establece el atributo id en "collapseOne"
                  div.setAttribute("id", "collapseOne"+o);
                  // establece la clase "collapse show"
                  // div.setAttribute("class", "collapse show");
                  div.setAttribute("class", "collapse");
                  //crea el elemento h2
                  var h2_1 = document.createElement("h2");
                  h2_1.setAttribute("class", "mb-0");

                  // crea el elemento button
                  var button3 = document.createElement("button");
                  button3.setAttribute("class", "btn btn-link");
                  button3.setAttribute("type", "button");
                  button3.setAttribute("data-toggle", "collapse");
                  button3.setAttribute("data-target", "#collapseOne"+o);
                  button3.setAttribute("aria-expanded", "false");
                  button3.setAttribute("aria-controls", "collapseOne");
                  button3.textContent = "Panel 1"+o;

                  // agrega el botón al elemento h2
                  h2_1.appendChild(button3);
                  var o =0;
                  console.log(data);
                  data.acapite.forEach(element2 => {
                    if(element2.capitulo_id==element.id){
                      var innerDiv = document.createElement("div");
                            innerDiv.setAttribute("class", "card-body");
                            let btn = document.createElement('button');
                            btn.classList.add('btn', 'btn-sm', 'btn-danger', 'ml-2');
                            btn.innerHTML = '<i class="fas fa-trash"></i>';
                            btn.setAttribute("data-id", element2.id);
                            btn.setAttribute("data-toggle", "modal");
                            btn.setAttribute("data-target", "#confirm-delete-modal");
                              //Agregar un evento click al botón
                              btn.addEventListener('click', function() {
                                  // Obtener el ID del botón
                                  let id = this.getAttribute('data-id');
                                  const btnEliminar = document.getElementById('confirm-delete-modal');
                                  btnEliminar.addEventListener('click', function() {
                                     
                                    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                            // Enviar una solicitud a la URL con el ID
                                        console.log(id);
                                      fetch('/modalidad/eliminar/acapite/' + id, {
                                          method: 'DELETE',
                                          headers: {
                                            'X-CSRF-TOKEN': token,
                                        }
                                          
                                      })
                                      .then(function(response) {
                                          if (!response.ok) {
                                              throw new Error('La eliminación falló');
                                          }
                                          return response.json(); // Obtener la respuesta JSON
                                      })
                                      .then(function(data) {
                                          // Mostrar un toast con el mensaje de éxito
                                          // Cerrar el modal y recargar la página después de 2 segundos
                                          setTimeout(function() {
                                              $('#confirm-delete-modal').modal('hide');
                                              location.reload();
                                          }, 2000);
                                      })
                                      .catch(function(error) {
                                          // Mostrar un toast con el mensaje de error
                                          toastr.error(error.message);
                                      });
                                  });
                              });
                            innerDiv.textContent = element2.capitulo_id+'.'+element2.indice+' '+element2.nombre;

                            // agrega el elemento interno al elemento div
                            innerDiv.appendChild(btn);
                            div.appendChild(innerDiv);
                            // agrega el elemento div al elemento con el id "accordionExample"
                            document.getElementById("estructura").appendChild(div);
                    }
                  });

                    
                
                });
                   
            });  
    });
    
  
    const botonAgregarOpciones = document.getElementById('editar');
    const selectOpciones = document.getElementById('select-opciones');
    botonAgregarOpciones.addEventListener('click', function() {
        let url = '/modalidad/'+idGlobal;
        let  miInput = document.getElementById("modalidad_id");
        miInput.value=idGlobal;
        fetch(url)
            .then(response => response.json())
            .then(data => {
              console.log("Esto es la data");
                console.log(data);
                selectOpciones.innerHTML='';
                data.capitulos.forEach(element => {
                    const nuevaOpcion = document.createElement('option');
                    console.log(element);
                    nuevaOpcion.value = element.id;
                    nuevaOpcion.textContent = element.nombre+' '+element.indice;
                    selectOpciones.appendChild(nuevaOpcion);
                });    
            }
            );  
    });
    const botonAgregarOpciones1 = document.getElementById('editar');
    const selectOpciones1= document.getElementById('select-acapite');
    botonAgregarOpciones1.addEventListener('click', function() {
        let url = '/modalidad/'+idGlobal;
        
        selectOpciones1.innerHTML='';
        fetch(url)
            .then(response => response.json())
            .then(data => {
              console.log('llegue');
                data.capitulos.forEach(element => {
                    
                    const nuevaOpcion = document.createElement('option');
                    nuevaOpcion.value = element.id;
                    nuevaOpcion.textContent = element.nombre+' '+element.indice;
                    selectOpciones1.appendChild(nuevaOpcion);
                });    
            }
            );  
    });
    const form = document.getElementById('miFormulario');
    const capituloInput = document.getElementById('capitulo');
    const opcionesSelect = document.getElementById('select-opciones');
    form.addEventListener('submit', function(event) {
    event.preventDefault(); // Prevenir el envío normal del formulario
    const token = this.querySelector('input[name="_token"]').value; // Obtener el token CSRF desde el formulario
    const formData = new FormData(this); // Obtener los datos del formulario
      console.log(token);
  fetch(this.action, { // Enviar los datos del formulario a la API
    method: 'POST',
    body: formData,
    headers: {
      'X-CSRF-TOKEN': token // Incluir el token CSRF en las cabeceras de la solicitud
    }
  })
  .then(response => response.json())
  .then(data => {
    // Agregar el nuevo capítulo al select
   
    opcionesSelect.innerHTML='';
    data.capitulos.forEach(element => {
        const option = document.createElement('option');
        option.value = element.id;
        option.text = element.nombre;
        opcionesSelect.add(option);
    });

    // Limpiar el campo de capítulo
    capituloInput.value = '';

    // Mostrar un mensaje de confirmación utilizando Toast de AdminLTE
    const toastBody = document.createElement('div');
    toastBody.classList.add('toast-body');
    toastBody.innerText = 'El capítulo se ha guardado correctamente.';

    const toast = document.createElement('div');
    toast.classList.add('toast');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');
    toast.setAttribute('data-delay', '5000');

    const toastHeader = document.createElement('div');
    toastHeader.classList.add('toast-header');
    toastHeader.innerHTML = `
      <i class="fas fa-check-circle mr-2"></i>
      <strong class="mr-auto">¡Éxito!</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span>
      </button>
    `;

    toast.appendChild(toastHeader);
    toast.appendChild(toastBody);

    const toastContainer = document.querySelector('.toast-container');
    toastContainer.appendChild(toast);

    const toastInstance = new bootstrap.Toast(toast);
    toastInstance.show();
  })
  .catch(error => {
    console.error(error);
  });
});
const form1 = document.getElementById('miFormulario1');
const acapiteInput1 = document.getElementById('acapite');
const indiceInput1 = document.getElementById('indice');
const opcionesSelect1 = document.getElementById('select-acapite');

form1.addEventListener('submit', function(event) {
  event.preventDefault(); // Prevenir el envío normal del formulario

  const token1 = this.querySelector('input[name="_token"]').value; // Obtener el token CSRF desde el formulario
  const formData1 = new FormData(this); // Obtener los datos del formulario

  fetch(this.action, { // Enviar los datos del formulario a la API
    method: 'POST',
    body: formData1,
    headers: {
      'X-CSRF-TOKEN': token1 // Incluir el token CSRF en las cabeceras de la solicitud
    }
  })
  .then(response => response.json())
  .then(data => {
    acapiteInput1.value = '';

    // Mostrar un mensaje de confirmación utilizando Toast de AdminLTE
    const toastBody1 = document.createElement('div');
    toastBody1.classList.add('toast-body');
    toastBody1.innerText = 'El capítulo se ha guardado correctamente.';

    const toast1 = document.createElement('div');
    toast1.classList.add('toast');
    toast1.setAttribute('role', 'alert');
    toast1.setAttribute('aria-live', 'assertive');
    toast1.setAttribute('aria-atomic', 'true');
    toast1.setAttribute('data-delay', '5000');

    const toastHeader1 = document.createElement('div');
    toastHeader1.classList.add('toast-header');
    toastHeader1.innerHTML = `
      <i class="fas fa-check-circle mr-2"></i>
      <strong class="mr-auto">¡Éxito!</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span>
      </button>
    `;

    toast1.appendChild(toastHeader1);
    toast1.appendChild(toastBody1);

    const toastContainer1 = document.querySelector('.toast-container');
    toastContainer1.appendChild(toast1);

    const toastInstance1 = new bootstrap.Toast(toast1);
    toastInstance1.show();
  })
  .catch(error => {
    console.error(error);
  });
});

    </script>
@stop