<?php

use App\Http\Controllers\ModalidadController;
use App\Http\Controllers\MateriaController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HestudiantesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

<<<<<<< HEAD
Route::get('/documentos',[DocumentoController::class,'index'])->name('documento.index');
Route::get('/documentos/editar',[DocumentoController::class,'editar'])->name('documento.editar');
Route::get('/documentos/{id}',[DocumentoController::class,'show'])->name('documento.show');
Route::post('/documento/capitulo',[DocumentoController::class,'createCapitulo'])->name('documento.capitulo');

Route::resource('materias', MateriaController::class);
Route::resource('hestudiantes', HestudiantesController::class);
require __DIR__.'/auth.php';
=======

Route::get('/modalidad', [ModalidadController::class, 'index'])->name('modalidad.index');
// Route::get('/modalidad/editar',[ModalidadController::class,'editar'])->name('documento.editar');
Route::get('/modalidad/{id}', [ModalidadController::class, 'show'])->name('modalidad.show');
Route::post('/modalidad/capitulo', [ModalidadController::class, 'createCapitulo'])->name('modalidad.capitulo');
Route::post('/modalidad/acapite', [ModalidadController::class, 'createAcapite'])->name('modalidad.acapite');
Route::delete('/modalidad/eliminar/acapite/{id}', [ModalidadController::class, 'destroyAcapite'])->name('modalidad.eliminar.acapite');

Route::resource('estudiantes', EstudianteController::class);

Route::resource('avances', AvanceController::class);

Route::get('/avances/upload/avance', [AvanceController::class, 'showFormAvance'])->name('avances.showFormAvance');
Route::post('/avances/{avance}/create', [AvanceController::class, 'storeAvance'])->name('avances.storeAvance');

Route::resource('/proyectos', ProyectoController::class); // add for Celedonio 

Route::get('proyectos/{proyecto}/revisiones/create', [RevisionController::class, 'createRevision'])->name('revisiones.createRevision');

Route::get('proyectos/{proyecto}/avance', [ProyectoController::class, 'showAvances'])->name('proyectos.showAvances');



Route::resource('revisiones', RevisionController::class);

Route::put('observaciones_capitulos/{id}/estado', [ObservacionCapituloController::class, 'updateEstado'])->name('observaciones_capitulos.update_estado');
Route::put('observaciones_acapites/{id}/estado', [ObservacionAcapiteController::class, 'updateEstado'])->name('observaciones_acapites.update_estado');

Route::get('/acapites/{capituloId}', [AcapiteController::class, 'getAcapites'])->name('acapites.getAcapites');


require __DIR__ . '/auth.php';
>>>>>>> 18935f4a4b7e100c253fd9dff0e9264264624930
